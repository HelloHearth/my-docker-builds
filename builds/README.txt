
# Bookworm
docker build builds/debian-bookworm/ -t ipnoz/bookworm
docker build builds/debian-bookworm-apache2/ -t ipnoz/bookworm/apache2
docker build builds/debian-bookworm-apache2-php8.2-fpm/ -t ipnoz/bookworm/apache2-php8.2-fpm
docker build builds/debian-bookworm-nodejs-18/ -t ipnoz/bookworm/nodejs-18

# Bookworm builder
docker build --no-cache builds/debian-bookworm/ -t ipnoz/bookworm \
    && docker build --no-cache builds/debian-bookworm-apache2/ -t ipnoz/bookworm/apache2 \
    && docker build --no-cache builds/debian-bookworm-apache2-php8.2-fpm/ -t ipnoz/bookworm/apache2-php8.2-fpm \
    && docker build --no-cache builds/debian-bookworm-nodejs-18/ -t ipnoz/bookworm/nodejs-18

# Bullseye
docker build builds/debian-bullseye/ -t ipnoz/bullseye
docker build builds/debian-bullseye-apache2/ -t ipnoz/bullseye/apache2
docker build builds/debian-bullseye-apache2-php7.4-fpm/ -t ipnoz/bullseye/apache2-php7.4-fpm
docker build builds/debian-bullseye-nodejs-14/ -t ipnoz/bullseye/nodejs-14
docker build builds/slice2php/ -t ipnoz/slice2php

# Bullseye builder
docker build --no-cache builds/debian-bullseye/ -t ipnoz/bullseye \
    && docker build --no-cache builds/debian-bullseye-apache2/ -t ipnoz/bullseye/apache2 \
    && docker build --no-cache builds/debian-bullseye-apache2-php7.4-fpm/ -t ipnoz/bullseye/apache2-php7.4-fpm \
    && docker build --no-cache builds/debian-bullseye-nodejs-14/ -t ipnoz/bullseye/nodejs-14 \
