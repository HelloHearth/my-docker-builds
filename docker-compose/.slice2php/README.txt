
# This folder is hidden with a period from the all-dir.sh script
# How to...

# Run the container
docker-composer up -d

# Enter the container
docker exec -it slice2php bash

# Run the slice2php bash script
./slice2php.sh

# This will produce all Murmur.php Ice slice definition in
# builds/slice2php/app/.output directory
