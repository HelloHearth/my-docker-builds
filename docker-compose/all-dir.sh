#!/bin/sh

# exec docker-compose command on all sub-directories

DIRS=$(ls -d $PWD/*/)

for dir in $DIRS; do
  echo "cd "$dir
  cd $dir/ || exit 2
  # check the directory has a docker-compose.yml file
  [ -f ./docker-compose.yml ] || [ -f ./docker-compose.yaml ] || exit 2
  docker-compose "$@"
done
